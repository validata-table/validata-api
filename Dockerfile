FROM python:3.9-slim-bullseye
LABEL maintainer="admin-validata@jailbreak.paris"

# Env variables that configure Python to run in a container:
# do not keep dependencies downloaded by "pip install"
ENV PIP_NO_CACHE_DIR=1
# do not write "*.pyc" files
ENV PYTHONDONTWRITEBYTECODE=1
# do not buffer input/output operations, displaying prints and log messages immediately
ENV PYTHONUNBUFFERED=1

WORKDIR /app

EXPOSE 5000

RUN pip install gunicorn==20.0.4

COPY requirements.txt .
RUN pip install  --requirement requirements.txt

COPY . .
RUN pip install --editable .

CMD gunicorn --bind 0.0.0.0:5000 validata_api:app
