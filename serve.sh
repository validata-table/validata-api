#! /bin/sh

export FLASK_APP=validata_api
export FLASK_ENV=development
export FLASK_RUN_PORT=5600
flask run --with-threads
