# CHANGELOG

## 0.7.5

- Fix opening-hours-py package version in requirements.txt
- Upgrade requirements (validata-core 0.9.7)

## 0.7.4

- Upgrade requirements (validata-core `0.9.6`)

## 0.7.3

- Fix build docker image: Upgrade opening-hours-py to version 0.5.6 in requirements

## 0.7.2

- Upgrade requirements (validata-core `0.9.4`)

## 0.7.1

- Upgrade requirements (validata-core `0.9.3`)

## 0.7.0

- Add 'header_case' parameter in API to specify header case-sensitive mode in source
- Upgrade requirements (validata-core 0.9.2)
- Update gitlab ci

## 0.6.4

- Upgrade requirements (run pip compile for all packages)

## 0.6.3

- Upgrade requirements (validata-core `0.8.6`)

## 0.6.2

- Upgrade requirements (validata-core `0.8.5`)

## 0.6.1

- Upgrage requirements (validata-core `0.8.3`)
- Improve docker build (lighter base image, fix inconsistencies)

## 0.6.0

- Upgrade validata-core requirements to `0.8.1`

## 0.5.6

- Upgrade validata-core requirements to `0.7.6` to fix https://git.opendatafrance.net/validata/validata-ui/-/issues/104

## 0.5.5

- Upgrade validata-core requirements to `0.7.5` to fix https://git.opendatafrance.net/validata/validata-ui/-/issues/102

## 0.5.4

- Upgrade validata-core requirement to `0.7.4` (frictionless-py `4.10.6`)

## 0.5.3

- Upgrade validata-core requirement to `0.7.3` (encoding exception)
- Better handle source error

## 0.5.2

- Upgrade validata-core requirement to `0.7.2` (fix UTF-8 BOM issue)

## 0.5.1

- Upgrade requirements

## 0.5.0

- Merge `towards_frictionless_4` branch into master
- Update requirements (validata-core 0.7.0)

## 0.5.0a5 (`towards_frictionless_4` branch)

- Update requirements (validata-core 0.7.0a7)
- Fix flake8 issues

## 0.5.0a4 (`towards_frictionless_4` branch)

- Update requirements (validata-core 0.7.0a6)

## 0.5.0a3 (`towards_frictionless_4` branch)

- Update requirements (frictionless 4.2.1)

## 0.5.0a2 (`towards_frictionless_4` branch)

- Update requirements (frictionless 4.1.0)

## 0.5.0a1 (`towards_frictionless_4` branch)

- Use validata-core 0.7.0alpha

## 0.4.0

- Update README

## 0.4.0a5

- install dev tools
- fix linter issues
- remove badge processing

## 0.4.0a4

- use setup.py for dependencies
- use published alpha versions of `validata-core`
- remove unused `BADGE_CONFIG_URL` config key

## 0.4.0a3

- update validata-core dependency (fix encoding issues)

## 0.4.0a2

- update validata-core dependency

## 0.4.0a1

- restore swagger ui
- use Black formatter
- fix flake8 issues

## 0.4.0a0

- upgrade to new validata-core (frictionless-py flavoured)
- refresh Dockerfile

## 0.3.8

- Fix flasgger dependencies

## 0.3.7

- Add forgotten flask-matomo dependency

## 0.3.6

- Update validata-core dependency

## 0.3.5

- Update validata-core dependency

## 0.3.4

- Update validata-core dependency (do not validate empty values)

## 0.3.3

- Update validata-core dependency (duplicate header support)

## 0.3.2

- Update validata-core dependency

## 0.3.1

- Update validata-core dependency

## 0.3.0

Breaking changes:

- In `/validate` endpoint, a third boolean parameter `repair` indicates if the tabular file structure
  has to be repaired prior to validate its content

## 0.2.3

Update validata-core dependency.

## 0.2.1

Technical changes: store app version in text file.

## 0.2.0

In short, this release separates Validata API from the [SCDL](https://scdl.opendatafrance.net/docs/) initiative.

Breaking changes:

- In `/validate` endpoint request: schema parameter is no more a SCDL tag but an URL to a `schema.json` file

## 0.1.1

Non-breaking changes:

- Respond `badge` property in `/validate` endpoint (optional, see `BADGE_CONFIG_URL` in `.env.example`)
